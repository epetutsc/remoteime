﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace RemoteIme.Services
{
    public class RemoteController : IDisposable
    {
        public const int DefaultPort = 7002;
        private readonly string _host;
        private readonly int _port;
        private Socket _socket;

        public RemoteController(string host, int port = DefaultPort)
        {
            _host = host;
            _port = port;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        ~RemoteController()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.Close();
            }
            _socket = null;
        }

        private Task ConnectAsync()
        {
            var tcs = new TaskCompletionSource<int>();
            var args = GetSocketAsyncEventArgs(tcs, 0);
            _socket.ConnectAsync(args);
            return tcs.Task;
        }

        private SocketAsyncEventArgs GetSocketAsyncEventArgs<T>(TaskCompletionSource<T> tcs, T result)
        {
            var args = new SocketAsyncEventArgs
            {
                RemoteEndPoint = new IPEndPoint(IPAddress.Parse(_host), _port),
            };
            args.Completed += (s, e) => tcs.SetResult(result);
            return args;
        }

        public async Task SendKeyStrokeAsync(KeyStroke keyStroke)
        {
            if (!_socket.Connected)
            {
                await ConnectAsync().ConfigureAwait(false);
            }
            var tcs = new TaskCompletionSource<int>();
            var args = GetSocketAsyncEventArgs(tcs, 0);
            args.SetBuffer(keyStroke, 0, keyStroke.Length);
            _socket.SendAsync(args);
            await tcs.Task.ConfigureAwait(false);
        }

        public Task DownUpAsync(KeyStroke.Type type)
        {
            var down = new KeyStroke(type, true);
            var up = new KeyStroke(type, false);
            return SendKeyStrokeAsync(down)
                .ContinueWith(t => SendKeyStrokeAsync(up));
        }

        public Task DownUpAsync(char ch)
        {
            var down = new KeyStroke(ch, true);
            var up = new KeyStroke(ch, false);
            return SendKeyStrokeAsync(down)
                .ContinueWith(t => SendKeyStrokeAsync(up));
        }

        public async Task Character(char ch)
        {
            if (!char.IsUpper(ch))
            {
                await DownUpAsync(ch);
            }
            else
            {
                ch = char.ToLower(ch);
                await ShiftDown();
                await DownUpAsync(ch);
                await ShiftUp();
            }
        }

        public Task ShiftDown()
        {
            var key = new KeyStroke(KeyStroke.Type.Shift, true);
            return SendKeyStrokeAsync(key);
        }

        public Task ShiftUp()
        {
            var key = new KeyStroke(KeyStroke.Type.Shift, false);
            return SendKeyStrokeAsync(key);
        }
    }
}
