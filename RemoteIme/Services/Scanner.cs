﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RemoteIme.Services
{
    public class Scanner
    {
        public class FoundEventArgs : EventArgs
        {
            public string Address { get; set; }
        }

        public const string DefaultGroup = "224.0.0.251"; // mDNS (UDP)
        public const int DefaultDetectionPort = 5353; // mDNS (UDP)

        public event EventHandler<FoundEventArgs> Found;

        public async Task<IEnumerable<string>> ScanAsync(TimeSpan timeout)
        {
            var result = new List<string>();
            using (var client = await CreateClient().ConfigureAwait(false))
            {
                var end = DateTime.UtcNow + timeout;
                for (var now = DateTime.UtcNow; now < end; now = DateTime.UtcNow)
                {
                    var delay = (end > now) ? (end - now) : TimeSpan.Zero;

                    var receiveTask = ReceiveTask(client);
                    var timeoutTask = Task.Delay(delay);

                    var finishedTask = await Task.WhenAny(timeoutTask, receiveTask);
                    if (finishedTask == receiveTask)
                    {
                        var ipAddress = receiveTask.Result;
                        if (ipAddress != null && !result.Contains(ipAddress))
                        {
                            result.Add(ipAddress);
                            if (Found != null)
                            {
                                Found(this, new FoundEventArgs
                                {
                                    Address = ipAddress
                                });
                            }
                        }
                    }
                }

                return result;
            }
        }

        private static Task<string> ReceiveTask(UdpAnySourceMulticastClient client)
        {
            var receiveBuffer = new byte[1024];
            var task = Task.Factory.FromAsync(
                client.BeginReceiveFromGroup,
                res => EndReceive(client, receiveBuffer, res),
                receiveBuffer, 0, receiveBuffer.Length, null);
            return task;
        }

        private static string EndReceive(UdpAnySourceMulticastClient client, byte[] receiveBuffer, IAsyncResult asyncResult)
        {
            try
            {
                IPEndPoint source;
                client.EndReceiveFromGroup(asyncResult, out source);

                var dataReceived = Encoding.UTF8.GetString(receiveBuffer, 0, receiveBuffer.Length);
                dataReceived = dataReceived.Replace('\0', ' ');
                if (dataReceived.Contains("anymote"))
                {
                    return source.Address.ToString();
                }
            }
            catch
            {
                // ignored
            }
            return null;
        }

        private static async Task<UdpAnySourceMulticastClient> CreateClient()
        {
            var client = new UdpAnySourceMulticastClient(IPAddress.Parse(DefaultGroup), DefaultDetectionPort);
            await JoinGroup(client);
            client.MulticastLoopback = false;
            return client;
        }

        private static async Task JoinGroup(UdpAnySourceMulticastClient client)
        {
            await Task.Factory.FromAsync(client.BeginJoinGroup, client.EndJoinGroup, null);
        }
    }
}
