﻿using System;

namespace RemoteIme.Error
{
    public class ErrorEventArgs
    {
        public Exception Exception { get; set; }
    }
}
