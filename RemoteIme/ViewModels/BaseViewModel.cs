﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RemoteIme.Error;

namespace RemoteIme.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region Get/Set/Unset
        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

        protected T Get<T>([CallerMemberName] string propertyName = null)
        {
            return propertyName != null && _properties.ContainsKey(propertyName) 
                ? (T) _properties[propertyName] 
                : default(T);
        }

        protected bool Set<T>(T value, [CallerMemberName] string propertyName = null)
        {
            if (propertyName != null)
            {
                // ReSharper disable once ExplicitCallerInfoArgument
                var oldValue = Get<T>(propertyName);
                if (!EqualityComparer<T>.Default.Equals(oldValue, value))
                {
                    _properties[propertyName] = value;
                    NotifyPropertyChanged(propertyName);
                    return true;
                }
            }
            return false;
        }

        protected bool Unset([CallerMemberName] string propertyName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            if (propertyName != null && _properties.ContainsKey(propertyName))
            {
                _properties.Remove(propertyName);
                return true;
            }
            return false;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Error
        public event EventHandler<ErrorEventArgs> Error;
        protected void RaiseError(Exception exception)
        {
            if (Error != null)
            {
                Error(this, new ErrorEventArgs { Exception = exception });
            }
        }
        #endregion

        #region Dispose

        protected void Dispose(object obj)
        {
            var disposable = obj as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
        #endregion
    }
}
