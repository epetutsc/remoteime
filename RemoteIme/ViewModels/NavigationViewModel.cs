﻿using System;
using System.Windows.Input;
using RemoteIme.Services;

namespace RemoteIme.ViewModels
{
    public class NavigationViewModel : BaseViewModel
    {
        #region fields
        private ICommand _sendKeyCommand;
        private ICommand _sendCharCommand;
        private RemoteController _remoteController;
        #endregion

        #region properties

        public bool IsDeviceAvailable
        {
            get { return _remoteController != null; }
        }
        #endregion

        #region commands

        public ICommand SendKeyCommand
        {
            get { return _sendKeyCommand ?? (_sendKeyCommand = new Command(SendKey)); }
        }

        public ICommand SendCharCommand
        {
            get { return _sendCharCommand ?? (_sendCharCommand = new Command(SendChar)); }
        }

        #endregion

        #region command operations
        private async void SendKey(object parameter)
        {
            if (parameter != null)
            {
                try
                {
                    var key = (KeyStroke.Type) Enum.Parse(typeof (KeyStroke.Type), (string) parameter);
                    await _remoteController.DownUpAsync(key);
                }
                catch (Exception e)
                {
                    RaiseError(e);
                }
            }
        }

        private async void SendChar(object parameter)
        {
            if (parameter != null)
            {
                try
                {
                    char ch;
                    if (char.TryParse(parameter.ToString(), out ch))
                    {
                        await _remoteController.Character(ch);
                    }
                }
                catch (Exception e)
                {
                    RaiseError(e);
                }
            }
        }
        #endregion

        #region operations
        public void ChangeDevice(string ipAddress)
        {
            Dispose(_remoteController);
            if (ipAddress != null)
            {
                _remoteController = new RemoteController(ipAddress);
            }
            NotifyPropertyChanged("IsDeviceAvailable");
        }
        #endregion
    }
}
