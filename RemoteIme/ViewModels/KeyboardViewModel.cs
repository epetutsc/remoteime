﻿using System;
using System.Windows.Input;
using RemoteIme.Services;
using System.Threading.Tasks;

namespace RemoteIme.ViewModels
{
    public class KeyboardViewModel : BaseViewModel
    {
        #region fields
        private RemoteController _remoteController;
        #endregion

        #region properties

        public bool IsDeviceAvailable
        {
            get { return _remoteController != null; }
        }
        #endregion

        #region operations
        public void ChangeDevice(string ipAddress)
        {
            Dispose(_remoteController);
            if (ipAddress != null)
            {
                _remoteController = new RemoteController(ipAddress);
            }
            NotifyPropertyChanged("IsDeviceAvailable");
        }
        #endregion

        public async Task Send(KeyStroke.Type type)
        {
            try
            {
                await _remoteController.DownUpAsync(type);
            }
            catch (Exception e)
            {
                RaiseError(e);
            }
        }

        public async Task Send(char character)
        {
            try
            {
                await _remoteController.Character(character);
            }
            catch (Exception e)
            {
                RaiseError(e);
            }
        }
    }
}
