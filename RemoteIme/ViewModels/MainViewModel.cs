﻿using System;
using RemoteIme.Error;

namespace RemoteIme.ViewModels
{
    public class MainViewModel
    {
        private DevicesViewModel _devicesViewModel;
        private NavigationViewModel _navigationViewModel;
        private KeyboardViewModel _keyboardViewModel;

        public DevicesViewModel DevicesViewModel
        {
            get { return _devicesViewModel ?? (_devicesViewModel = new DevicesViewModel()); }
        }

        public NavigationViewModel NavigationViewModel
        {
            get { return _navigationViewModel ?? (_navigationViewModel = new NavigationViewModel()); }
        }


        public KeyboardViewModel KeyboardViewModel
        {
            get { return _keyboardViewModel ?? (_keyboardViewModel = new KeyboardViewModel()); }
        }

        public event EventHandler<ErrorEventArgs> Error
        {
            add
            {
                DevicesViewModel.Error += value;
                NavigationViewModel.Error += value;
                KeyboardViewModel.Error += value;
            }
            remove
            {
                DevicesViewModel.Error -= value;
                NavigationViewModel.Error -= value;
                KeyboardViewModel.Error -= value;
            }
        }

        public MainViewModel()
        {
            DevicesViewModel.DeviceChanged += DevicesViewModel_DeviceChanged;
        }

        private void DevicesViewModel_DeviceChanged(object sender, EventArgs e)
        {
            if (DevicesViewModel.SelectedDevice != null)
            {
                NavigationViewModel.ChangeDevice(DevicesViewModel.SelectedDevice.Name);
                KeyboardViewModel.ChangeDevice(DevicesViewModel.SelectedDevice.Name);
            }
        }
    }
}
