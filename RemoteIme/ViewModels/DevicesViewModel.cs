﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using RemoteIme.Services;

namespace RemoteIme.ViewModels
{
    public class DevicesViewModel : BaseViewModel
    {
        #region events
        public event EventHandler DeviceChanged;
        private void RaiseDeviceChangedEvent()
        {
            if (DeviceChanged != null)
            {
                DeviceChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region fields
        private ICommand _scan;
        #endregion

        #region properties
        public ObservableCollection<DeviceViewModel> Devices { get; private set; }

        public ICommand Scan
        {
            get { return _scan ?? (_scan = new Command(ScanForDevices)); }
        }

        public Visibility ProgressVisibility 
        {
            get { return IsScanning ? Visibility.Visible : Visibility.Collapsed; }
        }

        public bool IsScanButtonEnabled
        {
            get { return !IsScanning; }
        }

        public DeviceViewModel SelectedDevice
        {
            get { return Get<DeviceViewModel>(); }
            set
            {
                if (Set(value))
                {
                    RaiseDeviceChangedEvent();
                }
            }
        }

        public bool IsScanning
        {
            get { return Get<bool>(); }
            set
            {
                if (Set(value))
                {
                    NotifyPropertyChanged("IsScanButtonEnabled");
                    NotifyPropertyChanged("ProgressVisibility");
                }
            }
        }
        #endregion

        #region construction
        public DevicesViewModel()
        {
            Devices = new ObservableCollection<DeviceViewModel>();
        }
        #endregion

        #region operations
        private async void ScanForDevices(object parameter)
        {
            try
            {
                IsScanning = true;
                var scanner = new Scanner();
                var found = await scanner.ScanAsync(TimeSpan.FromSeconds(30));
                SetDevices(found);
            }
            catch (Exception ex)
            {
                RaiseError(ex);
            }
            finally
            {
                IsScanning = false;
            }
        }

        private void SetDevices(IEnumerable<string> devices)
        {
            Devices.Clear();
            foreach (var device in devices)
            {
                Devices.Add(new DeviceViewModel
                {
                    IpAddress = device,
                    Name = device,
                });
            }

            var dev = Devices.FirstOrDefault();
            if (dev != null)
            {
                dev.IsSelected = true;
            }
            SelectedDevice = dev;
        }
        #endregion
    }
}
