﻿namespace RemoteIme.ViewModels
{
    public class DeviceViewModel : BaseViewModel
    {
        #region properties
        public string Name
        {
            get { return Get<string>(); }
            set { Set(value); }
        }

        public string IpAddress
        {
            get { return Get<string>(); }
            set { Set(value); }
        }

        public bool IsSelected
        {
            get { return Get<bool>(); }
            set { Set(value); }
        }
    }
    #endregion
}
