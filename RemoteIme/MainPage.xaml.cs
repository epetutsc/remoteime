﻿using System.Linq;
using RemoteIme.Services;
using RemoteIme.ViewModels;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace RemoteIme
{
    public partial class MainPage
    {
        private string _oldText;
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DataContext = App.ViewModel;
            App.ViewModel.Error += (s, e) => MessageBox.Show(e.Exception.Message);

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void txtKeyboard_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != Key.Shift)
            {
                var viewModel = App.ViewModel.KeyboardViewModel;
                switch(e.Key)
                {
                    case Key.Back: 
                        await viewModel.Send(KeyStroke.Type.Back);
                        return;
                    case Key.Delete:
                        await viewModel.Send(KeyStroke.Type.Del);
                        return;
                    case Key.Enter:
                        await viewModel.Send(KeyStroke.Type.Enter);
                        return;
                    case Key.Space:
                        await viewModel.Send(KeyStroke.Type.Space);
                        return;
                }

                var textBox = sender as TextBox;
                if (textBox != null && textBox.Text != null && textBox.Text.Length > 0 && _oldText != textBox.Text)
                {
                    _oldText = textBox.Text;
                    var ch = textBox.Text.Last();
                    await viewModel.Send(ch);
                }
            }
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}