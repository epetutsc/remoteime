﻿namespace RemoteControlTest
{
    public class KeyStroke
    {
        public enum Type
        {
            AlphaNumeric,
            Home,
            Menu,
            Back,
            VolumeUp,
            VolumeDown,
            Search,
            Ok,
            LeftArrow,
            UpArrow,
            RightArrow,
            DownArrow,
            FastBackward,
            FastForward,
            Play,
            Previous,
            Next,
            Shift,
            Space,
            Enter,
            Del,
            Tab,
            MouseClick,
        }

        public byte[] Bytes { get; private set; }

        public int Length
        {
            get
            {
                if (Bytes != null)
                {
                    return Bytes.Length;
                }
                return 0;
            }
        }

        public KeyStroke(Type type, bool isKeyDown)
        {
            //00:00:00:04:01:01:00:00
            Bytes = new byte[] { 0x00, 0x00, 0x00, 0x04, 0x01, 0x00, 0xff, 0x00 };
            SetType(type);
            SetKeyDown(isKeyDown);
        }

        public KeyStroke(char character, bool isKeyDown)
            : this(Type.AlphaNumeric, isKeyDown)
        {
            Bytes[6] = CharacterToByte(character);
        }

        private void SetKeyDown(bool isKeyDown)
        {
            Bytes[5] = isKeyDown ? (byte)0 : (byte)1;
        }

        private void SetType(Type type)
        {
            switch (type)
            {
                case Type.Back:
                    Bytes[6] = 0x02;
                    break;
                case Type.Del:
                    Bytes[6] = 0x45;
                    break;
                case Type.DownArrow:
                    Bytes[6] = 0x08;
                    break;
                case Type.Enter:
                    Bytes[6] = 0x44;
                    break;
                case Type.FastBackward:
                    Bytes[6] = 0x59;
                    break;
                case Type.FastForward:
                    Bytes[6] = 0x5a;
                    break;
                case Type.Home:
                    Bytes[6] = 0x00;
                    break;
                case Type.LeftArrow:
                    Bytes[6] = 0x09;
                    break;
                case Type.MouseClick:
                    Bytes[6] = 0xc9;
                    break;
                case Type.Menu:
                    Bytes[6] = 0x01;
                    break;
                case Type.Next:
                    Bytes[6] = 0x57;
                    break;
                case Type.Ok:
                    Bytes[6] = 0x0b;
                    break;
                case Type.Play:
                    Bytes[6] = 0x55;
                    break;
                case Type.Previous:
                    Bytes[6] = 0x58;
                    break;
                case Type.RightArrow:
                    Bytes[6] = 0x0a;
                    break;
                case Type.Search:
                    Bytes[6] = 0x05;
                    break;
                case Type.Shift:
                    Bytes[6] = 0x3d;
                    break;
                case Type.Space:
                    Bytes[6] = 0x40;
                    break;
                case Type.Tab:
                    Bytes[6] = 0x3f;
                    break;
                case Type.UpArrow:
                    Bytes[6] = 0x07;
                    break;
                case Type.VolumeDown:
                    Bytes[6] = 0x04;
                    break;
                case Type.VolumeUp:
                    Bytes[6] = 0x03;
                    break;
                default:
                    Bytes[6] = 0xff;
                    break;
            }
        }

        private byte CharacterToByte(char ch)
        {
            if (ch >= 'a' && ch <= 'z')
            {
                return (byte)(0x1f + (ch - 'a'));
            }

            if (ch >= 'A' && ch <= 'Z')
            {
                return (byte)(0x83 + (ch - 'a'));
            }

            if (ch >= '0' && ch <= '9')
            {
                return (byte)(0x13 + (ch - 'a'));
            }

            switch (ch)
            {
                case '@':
                    return 0x4f;
                case '-':
                    return 0x4f;
                case ',':
                    return 0x39;
                case '.':
                    return 0x3a;
                case '#':
                    return 0x1e;
                case '*':
                    return 0x1d;
                case '+':
                    return 0x53;
                case '[':
                    return 0x49;
                case ']':
                    return 0x4a;
                case '=':
                    return 0x48;
                case '`':
                    return 0x46;
                case ';':
                    return 0x4c;
            }

            return 0xff;
        }

        public static implicit operator byte[] (KeyStroke keyStroke)
        {
            return keyStroke.Bytes;
        }
    }
}
