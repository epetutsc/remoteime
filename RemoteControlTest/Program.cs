﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace RemoteControlTest
{
    class Program
    {
        static void Main()
        {
            Run().Wait();
        }

        private async static Task Run()
        {
            await SendTest();
        }

        private static void AddressFound(object sender, Scanner.FoundEventArgs e)
        {
            Console.WriteLine(e.Address);
        }

        private static async Task SendTest()
        {
            RemoteController.DeviceFound += AddressFound;
            var result = await RemoteController.FindDeviceAsync();
            RemoteController.DeviceFound -= AddressFound;

            var ipAddress = result.FirstOrDefault();
            if (ipAddress != null)
            {
                using (var controller = new RemoteController(ipAddress))
                {
                    Console.WriteLine("<Back>");
                    controller.Back();
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    Console.WriteLine("<Home>");
                    controller.Home();
                }
            }
        }
    }
}
