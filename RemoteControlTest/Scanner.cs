﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace RemoteControlTest
{
    public class Scanner
    {
        public class FoundEventArgs : EventArgs
        {
            public string Address { get; set; }
        }

        public const int DefaultDetectionPort = 5353; // mDNS (UDP)

        public event EventHandler<FoundEventArgs> Found;

        public async Task<IEnumerable<string>> ScanAsync(TimeSpan timeout)
        {
            return await Task.Factory.StartNew(() => Receive(timeout));
        }

        private IEnumerable<string> Receive(TimeSpan timeout)
        {
            var result = new List<string>();
            using (var udp = new UdpClient())
            {
                var rcvEndPoint = new IPEndPoint(IPAddress.Any, DefaultDetectionPort);
                udp.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                udp.Client.Bind(rcvEndPoint);
                udp.JoinMulticastGroup(IPAddress.Parse("224.0.0.251"), 255);

                var start = DateTime.UtcNow;
                while (start + timeout > DateTime.UtcNow)
                {
                    var data = udp.Receive(ref rcvEndPoint);
                    var str = ProcessReceivedData(data);
                    if (str.Contains("anymote"))
                    {
                        var address = rcvEndPoint.Address.ToString();
                        if (!result.Contains(address))
                        {
                            result.Add(address);
                            if (Found != null)
                            {
                                Found(this, new FoundEventArgs
                                {
                                    Address = address
                                });
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static string ProcessReceivedData(byte[] data)
        {
            var characters = new char[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                var b = data[i];
                var ch = Convert.ToChar(b);
                if (ch == '\0')
                {
                    ch = ' ';
                }
                characters[i] = ch;
            }
            return new string(characters);
        }
    }
}
