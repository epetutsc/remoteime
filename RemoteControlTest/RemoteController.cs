﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace RemoteControlTest
{
    public class RemoteController : IDisposable
    {
        public const int DefaultPort = 7002;

        private static readonly Scanner Scanner = new Scanner();

        private readonly string _host;
        private readonly int _port;
        private readonly TcpClient _remote;

        public static event EventHandler<Scanner.FoundEventArgs> DeviceFound
        {
            add { Scanner.Found += value; }
            remove { Scanner.Found -= value; }
        }

        public RemoteController(string host, int port = DefaultPort)
        {
            _host = host;
            _port = port;
            _remote = new TcpClient();
        }

        #region IDisposable
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~RemoteController()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
            }

            if (_remote.Connected)
            {
                _remote.Close();
            }

            _disposed = true;
        }

        #endregion

        private void SendKeyStroke(KeyStroke keyStroke)
        {
            if (!_remote.Connected)
            {
                _remote.Connect(_host, _port);
            }
            _remote.Client.Send(keyStroke, keyStroke.Length, SocketFlags.None);
        }

        private void DownUp(KeyStroke.Type type)
        {
            var down = new KeyStroke(type, true);
            var up = new KeyStroke(type, false);
            SendKeyStroke(down);
            SendKeyStroke(up);
        }

        public static async Task<IEnumerable<string>> FindDeviceAsync()
        {
            return await Scanner.ScanAsync(TimeSpan.FromSeconds(30));
        }

        public void Close()
        {
            try
            {
                _remote.Close();
            }
            catch
            {
                // ignored
            }
        }

        public void Home()
        {
            DownUp(KeyStroke.Type.Home);
        }

        public void Back()
        {
            DownUp(KeyStroke.Type.Back);
        }


        
    }
}
